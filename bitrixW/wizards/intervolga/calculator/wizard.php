<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 *  Шаг 1.
 */
class SignSelection extends CWizardStep{

	function InitStep(){
		$this->SetStepID("SignSelection");

		$this->SetTitle(GetMessage("SIGN_SELECTION_INIT_TITLE"));
		$this->SetSubTitle(GetMessage("SIGN_SELECTION_INIT_SUBTITLE"));

		$this->SetNextStep("EnterFirstNumber");
	}

	function ShowStep(){
        $this->content .= "<p>";
        $this->content .= GetMessage("SHOW_STEP_CHOICE");
        $this->content .= $this->ShowSelectField(
            "operation",
            Array(
                "add" => GetMessage("SHOW_STEP_ADD"),
                "subtract" => GetMessage("SHOW_STEP_SUBTRACT"),
                "multiply" => GetMessage("SHOW_STEP_MULTIPLY"),
                "divide" => GetMessage("SHOW_STEP_DIVIDE"),
            ),
        );
        $this->content .= "<p>";
	}

}

/**
 * Шаг 2.
 */
class EnterFirstNumber extends CWizardStep{

	function InitStep(){
        $this->SetStepID("EnterFirstNumber");

        $this->SetTitle(GetMessage("ENTER_FIRST_NUMBER_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("ENTER_FIRST_NUMBER_INIT_SUBTITLE"));

        $this->SetPrevStep("SignSelection");
        $this->SetNextStep("EnterSecondNumber");
	}
	
	function ShowStep()
    {
        $this->content .=
            GetMessage("SHOW_STEP_FIRST_NUMBER").$this->ShowInputField("text", "firstNumber");
    }

	
	function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        $firstNumber = $wizard->GetVar("firstNumber");

        if(!is_numeric($firstNumber))
        {
            $this->SetError(GetMessage("ON_POST_FORM_FIRST_NUMBER_ERROR"));
        }
    }
}

/**
 * Шаг 3.
 */
class EnterSecondNumber extends CWizardStep{

    function InitStep(){
        $this->SetStepID("EnterSecondNumber");

        $this->SetTitle(GetMessage("ENTER_SECOND_NUMBER_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("ENTER_SECOND_NUMBER_INIT_SUBTITLE"));

        $this->SetPrevStep("EnterFirstNumber");
        $this->SetNextStep("ShowOrError");
    }

    function ShowStep()
    {
        $this->content .=
            GetMessage("SHOW_STEP_SECOND_NUMBER").$this->ShowInputField("text", "secondNumber");

    }


    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        $firstNumber = $wizard->GetVar("firstNumber");
        $secondNumber = $wizard->GetVar("secondNumber");
        $operation = $wizard->GetVar("operation");
        $sign = "";
        $divideError = false;
        $result = 0;

        if(!is_numeric($secondNumber))
            $this->SetError(GetMessage("ON_POST_FORM_SECOND_NUMBER_ERROR"));

        switch($operation)
        {
            case "add":
                $sign = "+";
                $result = $firstNumber + $secondNumber;
                break;
            case "subtract":
                $sign = "-";
                $result = $firstNumber - $secondNumber;
                break;
            case "multiply":
                $sign = "*";
                $result = $firstNumber * $secondNumber;
                break;
            case "divide":
                $sign = "/";
                if ($secondNumber == 0)
                    $divideError = true;
                else
                    $result = $firstNumber/$secondNumber;
                break;
        }

        $wizard->SetVar("result",$result);
        $wizard->SetVar("sign",$sign);
        $wizard->SetVar("divideError",$divideError);
    }
}

/**
 * Шаг 4.
 */
class ShowOrError extends CWizardStep{

    function InitStep(){
        $this->SetStepID("ShowOrError");

        $this->SetTitle(GetMessage("SHOW_OR_ERROR_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("SHOW_OR_ERROR_INIT_SUBTITLE"));

        $this->SetPrevStep("EnterSecondNumber");
        $this->SetFinishStep("CalculateAndAddRecord");

    }

    function ShowStep()
    {
        $wizard =& $this->GetWizard();
        $firstNumber = $wizard->GetVar("firstNumber");
        $secondNumber = $wizard->GetVar("secondNumber");
        $sign = $wizard->GetVar("sign");
        $divideError = $wizard->GetVar("divideError");

        if($divideError)
        {
            $this->SetError(GetMessage("SHOW_STEP_DIVISION_ERROR"));
        }

        $this->content .= GetMessage("SHOW_STEP_SHOW_SAMPLE").$firstNumber." ".$sign." ". $secondNumber;
    }


    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        $divideError = $wizard->GetVar("divideError");
        if($divideError)
            $wizard->SetCurrentStep("EnterSecondNumber");

    }
}

/**
 * Шаг 5.
 */
class CalculateAndAddRecord extends CWizardStep{

    function InitStep(){
        $this->SetStepID("CalculateAndAddRecord");

        $this->SetTitle(GetMessage("RESULT_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("RESULT_INIT_SUBTITLE"));

        $this->SetCancelStep("CalculateAndAddRecord");
        $this->SetCancelCaption(GetMessage("RESULT_INIT_CAPTION_BUTTON"));
    }

    function ShowStep()
    {
        $wizard = &$this->GetWizard();
        $result = $wizard->GetVar("result");
        $this->content .= GetMessage("SHOE_STEP_RESULT").$result;

    }


    function OnPostForm()
    {
        $wizard = &$this->GetWizard();
        $result = $wizard->GetVar("result");
        CEventLog::Add(Array(
            "SEVERITY" => "INFO",
            "AUDIT_TYPE_ID" => "MASTER_RESULT",
            "MODULE_ID" => "main",
            "ITEM_ID" => "",
            "DESCRIPTION" => GetMessage("SHOE_STEP_RESULT").$result
        ));
    }
}

