<?
$MESS['SIGN_SELECTION_INIT_TITLE'] = "Действие";
$MESS['SIGN_SELECTION_INIT_SUBTITLE'] = "Действие из списка";
$MESS['SHOW_STEP_CHOICE'] = "Выберите действие из списка: ";
$MESS['SHOW_STEP_ADD'] = "сложение";
$MESS['SHOW_STEP_SUBTRACT'] = "вычитание";
$MESS['SHOW_STEP_MULTIPLY'] = "умножение";
$MESS['SHOW_STEP_DIVIDE'] = "деление";

$MESS['ENTER_FIRST_NUMBER_INIT_TITLE'] = "Первое число";
$MESS['ENTER_FIRST_NUMBER_INIT_SUBTITLE'] = "Введите первое число";
$MESS['SHOW_STEP_FIRST_NUMBER'] = "Первое число: ";
$MESS['ON_POST_FORM_FIRST_NUMBER_ERROR'] = "Необходимо ввести число";

$MESS['ENTER_SECOND_NUMBER_INIT_TITLE'] = "Второе число";
$MESS['ENTER_SECOND_NUMBER_INIT_SUBTITLE'] = "Введите второе число";
$MESS['SHOW_STEP_SECOND_NUMBER'] = "Второе число: ";
$MESS['ON_POST_FORM_SECOND_NUMBER_ERROR'] = "Необходимо ввести число";

$MESS['SHOW_OR_ERROR_INIT_TITLE'] = "Пример";
$MESS['SHOW_OR_ERROR_INIT_SUBTITLE'] = "Показывается пример";
$MESS['SHOW_STEP_DIVISION_ERROR'] = "Деление на ноль";
$MESS['SHOW_STEP_SHOW_SAMPLE'] = "Пример выглядит так: ";

$MESS['RESULT_INIT_TITLE'] = "Результат";
$MESS['RESULT_INIT_SUBTITLE'] = "Показывается результат";
$MESS['RESULT_INIT_CAPTION_BUTTON'] = "Записать результат в журнал и выйти";
$MESS['SHOE_STEP_RESULT'] = "Результат вычисления: ";