<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */

use	Bitrix\Main\Loader,
    Bitrix\Iblock;

//основной блок $arParams
if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);

//Метод поддержки внутреннего кеширования компонента
if($arParams["IBLOCK_ID"] > 0)
{
    if($this->startResultCache())
    {
        if(!Loader::includeModule("iblock"))
        {
            $this->abortResultCache();
            ShowError(GetMessage("COMPONENT_MODULE_EXISTENCE"));
            return;
        }

        $arResult["IBLOCK_TYPE_ID"] = $arParams["IBLOCK_TYPE"];
        $arResult["ID"] = $arParams["IBLOCK_ID"];

        //SELECT
        $arSelect = Array(
            "ID",
            "NAME",
            "PROPERTY_EXPERIENCE",
            "PROPERTY_SCHEDULE",
            "PROPERTY_EDUCATION",
            "PREVIEW_TEXT",
            "IBLOCK_SECTION_ID",
            "DETAIL_PAGE_URL",
        );

        //WHERE
        $arFilter = array (
            "IBLOCK_ID" => $arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y",
        );

        //сборка массив $arParams
        $arResult["ITEMS"] = Array();
        $arResult["SECTIONS_NAMES"] = Array();
        $sections = Array();

        $rsElement = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
        while ($row = $rsElement->GetNext())
        {
            $id = (int)$row['ID'];
            $iBlockSelectionId = (int)$row['IBLOCK_SECTION_ID'];
            $arResult["ITEMS"][$iBlockSelectionId][$id] = $row;

            $arButtons = CIBlock::GetPanelButtons(
                $row["IBLOCK_ID"],
                $row["ID"],
                0,
                array("SECTION_BUTTONS" => true, "SESSID" => false)
            );

            $arResult["ITEMS"][$iBlockSelectionId][$id]["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $arResult["ITEMS"][$iBlockSelectionId][$id]["DELETE_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];


            if(!in_array($iBlockSelectionId,$sections))
                $sections[] = $iBlockSelectionId;

        }
        unset($row);

        if($sections)
        {
            $sectionsNames = Array();

            $arSelect = Array("ID", "NAME");
            $arFilter = Array("IBLOCK_ID" => $arResult["ID"], "ID" => $sections);
            $res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
            while($arRes = $res->GetNext())
            {
                $sectionsNames[$arRes["ID"]] = $arRes["NAME"];
            }

            $arResult["SECTIONS_NAMES"] = $sectionsNames;
        }
        else
        {
            $arResult["ITEMS"] = "";
            $arResult["SECTIONS_NAMES"] = "";
            ShowError("Пока нет вакансий!");
        }

        //Список ключей массива $arResult, которые должны кэшироваться:
        //$arResult['ID']
        //$arResult['IBLOCK_TYPE_ID']
        //$arResult['ITEMS']
        //$arResult['SECTIONS_NAMES']
        $this->setResultCacheKeys(array(
            "ID",
            "IBLOCK_TYPE_ID",
            "SECTIONS_NAMES",
            "ITEMS",
        ));

        //Подключение шаблона
        $this->includeComponentTemplate();
    }
}
else
{
    ShowError(GetMessage("COMPONENT_ID_BLOCK_EXISTENCE"));
    return;
}