<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("DESCRIPTION_LIST_VACANCIES_NAME"),
    "DESCRIPTION" => GetMessage("DESCRIPTION_LIST_VACANCIES_DESCRIPTION"),
    "PATH" => array(
        "ID" => "test_vacancy",
        "NAME" => GetMessage("DESCRIPTION_LIST_VACANCIES_PATH"),
    ),
);

?>