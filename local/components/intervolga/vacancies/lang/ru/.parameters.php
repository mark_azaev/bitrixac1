<?
$MESS["VACANCY_ELEMENT_ID_DESC"] = "Идентификатор Вакансии";
$MESS["FORM_ELEMENT_ID_DESC"] = "Идентификатор Формы";
$MESS["SEF_MODE_VACANCIES_NAME"] = "Главная страница";
$MESS["SEF_MODE_VACANCY_DETAIL_NAME"] = "Страница детального просмотра";
$MESS["SEF_MODE_RESUME_NAME"] = "Cтраница отправки резюме на вакансию";
$MESS["BN_P_IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["BN_P_IBLOCK"] = "Инфоблок";
$MESS["T_WEB_FORM_ID"] = "ID Вебформы";
$MESS["BN_P_CACHE_FILTER"] = "Кешировать при установленном фильтре";
$MESS["CP_BN_CACHE_GROUPS"] = "Учитывать права доступа";
?>