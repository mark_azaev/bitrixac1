<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arDefaultUrlTemplates404 = array(
	"vacancies" => "",
	"vacancy" => "#VACANT_ID#/",
	"resume" => "#VACANT_ID#/resume/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"VACANT_ID",
);



if($arParams["SEF_MODE"] == "Y")
{
    $arVariables = array();

	$arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"],$arUrlTemplates,$arVariables);

	if(!$componentPage)
	{
		$componentPage = "vacancies";
	}

	CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
	);
}
else
{
    $arVariables = array();

	$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "";

    if(isset($arVariables["VACANT_ID"]) && intval($arVariables["VACANT_ID"]) > 0)
    {
        if(isset($arVariables["FORM_ID"]) && intval($arVariables["FORM_ID"]) > 0)
        {
            $componentPage = "resume";
        }
        else
        {
            $componentPage = "vacancy";
        }
    }
    else
    {
        $componentPage = "vacancies";
    }


	$arResult = array(
		"FOLDER" => "",
		"URL_TEMPLATES" => array(
			"vacancies" => htmlspecialcharsbx($APPLICATION->GetCurPage()),
			"vacancy" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["#VACANT_ID#"]."=#VACANT_ID#"),
			"resume" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["#FORM_ID#"]."=#FORM_ID#"),
        ),
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
}

$this->includeComponentTemplate($componentPage);