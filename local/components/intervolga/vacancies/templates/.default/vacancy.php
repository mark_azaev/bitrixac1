<?php
    if($arParams["SEF_MODE"] == "Y")
    {
        $arResult["LINK_TO_RESUME"] =
            str_replace($arResult["URL_TEMPLATES"]["vacancy"], '', $arResult["URL_TEMPLATES"]["resume"]);
    }
    else
    {
        $arResult["LINK_TO_RESUME"] = "&".$arResult["ALIASES"]["FORM_ID"]."=".$arParams["WEB_FORM_ID"];
    }
?>

<?$APPLICATION->IncludeComponent("bitrix:news.detail",".default",Array(
        "COMPONENT_TEMPLATE" => ".default",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "SEF_MODE" => $arParams["SEF_MODE"],
        "WEB_FORM_ID" => $arParams["WEB_FORM_ID"],
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_ID" => $arResult["VARIABLES"]["VACANT_ID"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "PROPERTY_CODE" => Array("EXPERIENCE","SCHEDULE","EDUCATION"),
        "LINKING" => $arResult["LINK_TO_RESUME"],
    ),
$component
);?>