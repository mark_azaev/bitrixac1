<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $sectionId => $vacancies):?>
    <div class="vc_content">
        <h2><?=$arResult["SECTIONS_NAMES"][$sectionId];?></h2>
        <?foreach($vacancies as $vacancy):?>
            <?
            $this->AddEditAction($vacancy['ID'], $vacancy['EDIT_LINK'], CIBlock::GetArrayByID($vacancy['ID'], "ELEMENT_EDIT"));
            $this->AddDeleteAction($vacancy['ID'], $vacancy['DELETE_LINK'], CIBlock::GetArrayByID($vacancy['ID'], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("TEMPLATE_DELETE_QUESTION")));
            ?>
        <ul class="vacancies" id="<?=$this->GetEditAreaId($vacancy['ID']);?>">
            <li class="close"><a href="<?=$vacancy["DETAIL_PAGE_URL"]?>"><h3><?=$vacancy['NAME']?></h3></a>
                <span class="vc_showchild"><?=GetMessage("TEMPLATE_MORE_DETAILS");?></span>
                <ul>
                    <li>
                        <strong><?=GetMessage("TEMPLATE_REVIEW_VACANCY");?></strong>
                        <br/><?=$vacancy['PREVIEW_TEXT']?>
                        <br/>
                        <br/><strong><?=GetMessage("TEMPLATE_EXPERIENCE");?></strong>
                        <br/><?=$vacancy['PROPERTY_EXPERIENCE_VALUE']?>
                        <br/>
                        <br/><strong><?=GetMessage("TEMPLATE_SCHEDULE");?></strong>
                        <br/><?=$vacancy['PROPERTY_SCHEDULE_VALUE']?>
                        <br/>
                        <br/><strong><?=GetMessage("TEMPLATE_EDUCATION");?></strong>
                        <br/><?=$vacancy['PROPERTY_EDUCATION_VALUE']?>
                    </li>
                </ul>
                <span class="vc_showchild-2 close"><?=GetMessage("TEMPLATE_HIDE_DETAILS");?></span>
            </li>
        </ul>
        <?endforeach;?>
    </div>
<?endforeach;?>

