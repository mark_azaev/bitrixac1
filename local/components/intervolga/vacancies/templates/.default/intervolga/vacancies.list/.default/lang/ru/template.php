<?php
$MESS["TEMPLATE_MORE_DETAILS"] = "Подробнее";
$MESS["TEMPLATE_REVIEW_VACANCY"] = "Описание вакансии:";
$MESS["TEMPLATE_EXPERIENCE"] = "Требуемый стаж:";
$MESS["TEMPLATE_SCHEDULE"] = "График работы:";
$MESS["TEMPLATE_EDUCATION"] = "Образование:";
$MESS["TEMPLATE_HIDE_DETAILS"] = "Скрыть подробности";
$MESS["TEMPLATE_DELETE_QUESTION"] = "Удалить?";
