<?php
$MESS["TEMPLATE_NAME_VACANCY"] = "Название вакансии: ";
$MESS["TEMPLATE_REVIEW_VACANCY"] = "Описание вакансии:";
$MESS["TEMPLATE_SCHEDULE"] = "График работы:";
$MESS["TEMPLATE_EDUCATION"] = "Образование:";
$MESS["TEMPLATE_EXPERIENCE"] = "Требуемый стаж:";
$MESS["TEMPLATE_SEND"] = "Отправить резюме на вакансию";