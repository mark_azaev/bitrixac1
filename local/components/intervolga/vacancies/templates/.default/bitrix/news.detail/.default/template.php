<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
$this->setFrameMode(true);
?>
<div class="news-detail">
    <h3><?=GetMessage("TEMPLATE_NAME_VACANCY").$arResult["NAME"];?></h3>
    <p>
        <?=GetMessage("TEMPLATE_REVIEW_VACANCY")?>
        <?echo $arResult["PREVIEW_TEXT"];?>
        <br>
        <?=GetMessage("TEMPLATE_SCHEDULE")?>
        <?echo $arResult["PROPERTIES"]["SCHEDULE"]["VALUE"];?>
        <br>
        <?=GetMessage("TEMPLATE_EDUCATION")?>
        <?echo $arResult["PROPERTIES"]["EDUCATION"]["VALUE"];?>
        <br>
        <?=GetMessage("TEMPLATE_EXPERIENCE")?>
        <?echo $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"];?>
    </p>
    <p>
        <a href="<?=$arResult["DETAIL_PAGE_URL"].$arParams["LINKING"]?>"><?=GetMessage("TEMPLATE_SEND")?></a>
    </p>
</div>