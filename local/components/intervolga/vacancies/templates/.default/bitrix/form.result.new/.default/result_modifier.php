<?php
/** @var array $arParams */
/** @var array $arResult */

if (CModule::IncludeModule("iblock"))
{
    if(!$arParams["ELEMENT_ID"])
    {
        LocalRedirect("/company/vacancies/");
        exit;
    }

    $vacancySelect = Array("NAME");
    $vacancyFilter = Array("ID" => $arParams["ELEMENT_ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
    $resultQuestion = CIBlockElement::GetList(Array(), $vacancyFilter, false, false, $vacancySelect);
    if($nameVacancy = $resultQuestion->Fetch())
    {
        $arResult["NAME_VACANCY"] = $nameVacancy["NAME"];
    }
}

$questionFilter = Array("COMMENTS" => VACANCY_CODE);
$questionField = CFormField::GetList($arParams["WEB_FORM_ID"], "N", $by="s_id", $order="desc", $questionFilter);
if($question = $questionField->Fetch())
{
    $SIDQuestion = $question["SID"];
}

foreach ($arResult["QUESTIONS"] as $idQuestion => $question)
{
    if($idQuestion == $SIDQuestion)
    {
        $question["HTML_CODE"] = <<<EOD
        <input  readonly="readonly" 
                type="text" 
                class="inputtext" 
                name="form_text_{$question["STRUCTURE"]["0"]["ID"]}"
                value="{$arParams["ELEMENT_ID"]} == {$arResult["NAME_VACANCY"]}">
        EOD;

        $arResult["QUESTIONS"][$idQuestion]["HTML_CODE"] = $question["HTML_CODE"];
    }
}

?>
