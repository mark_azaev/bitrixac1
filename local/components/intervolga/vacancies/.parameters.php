<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("SORT" => "ASC"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}


$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"VARIABLE_ALIASES" => Array(
			"VACANT_ID" => Array("NAME" => GetMessage("VACANCY_ELEMENT_ID_DESC")),
			"FORM_ID" => Array("NAME" => GetMessage("FORM_ELEMENT_ID_DESC")),
		),
		"SEF_MODE" => Array(
            "vacancies" => array(
                "NAME" => GetMessage("SEF_MODE_VACANCIES_NAME"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "vacancy" => array(
                "NAME" => GetMessage("SEF_MODE_VACANCY_DETAIL_NAME"),
                "DEFAULT" => "#VACANT_ID#/",
                "VARIABLES" => array(),
            ),
            "resume" => array(
                "NAME" => GetMessage("SEF_MODE_RESUME_NAME"),
                "DEFAULT" => "#VACANT_ID#/resume/",
                "VARIABLES" => array(),
            ),
		),
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("BN_P_IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("BN_P_IBLOCK"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
			"ADDITIONAL_VALUES" => "Y",
		),
        "WEB_FORM_ID" => Array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("T_WEB_FORM_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
		"CACHE_FILTER" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("BN_P_CACHE_FILTER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("CP_BN_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);
