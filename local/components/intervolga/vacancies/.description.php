<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("IBLOCK_VACANCY_NAME"),
    "DESCRIPTION" => GetMessage("IBLOCK_VACANCY_DESCRIPTION"),
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "test_comp_vacancy",
        "NAME" => GetMessage("DISPLAY_VACANCY_NAME"),
    ),
);

?>