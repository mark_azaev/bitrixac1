<?php

$arGroupBy = Array("PROPERTY_MATERIAL");
$arFilter = Array("IBLOCK_ID" => ID_CATALOG,"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, $arGroupBy, false, false);

while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arrMat[] = Array(
        "MATERIAL" => $arFields["PROPERTY_MATERIAL_VALUE"],
        "CNT" => $arFields["CNT"],
    );

}
$arResult["PROPERTY_MATERIAL_VALUE"] = $arrMat;

?>