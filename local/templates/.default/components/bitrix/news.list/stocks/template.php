<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>

<div class="sb_action">
    <a href=""><img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"></a>
    <h4>Акция</h4>
    <h5><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["PREVIEW_TEXT"]?></a></h5>
    <h5><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
            <?=$arItem["NAME"]?> за
            <?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE']?>
        </a></h5>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="sb_action_more">Подробнее &rarr;</a>
</div>


<?endforeach;?>








