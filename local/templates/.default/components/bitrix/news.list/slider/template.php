<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="sl_slider" id="slides">
    <div class="slides_container">
        <?foreach($arResult["ITEMS"] as $arItem):
          $catalogArray = $arResult["CAT_ELEM"][$arItem['PROPERTIES']['LINK_CAT']['VALUE']]; ?>
        <div>
            <div>
                <a title="<?=$catalogArray['NAME']?>"
                   href="<?=$catalogArray['DETAIL_PAGE_URL']?>">
                    <img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" alt="" />
                </a>
                <h2>
                    <?=$catalogArray['NAME']?>
                </h2>
                <p>
                    <?=$catalogArray['NAME']?>
                    всего за
                    <?=$catalogArray['PROPERTY_PRICE_VALUE']?>
                    руб.
                </p>
                <a href="<?=$catalogArray['DETAIL_PAGE_URL']?>" class="sl_more">Подробнее &rarr;</a>
            </div>
        </div>
        <?endforeach;?>
    </div>
</div>

<script type="text/javascript" >
    $().ready(function(){
        $(function(){
            $('#slides').slides({
                preload: true,
                generateNextPrev: false,
                autoHeight: true,
                play: 4000,
                effect: 'fade'
            });
        });
    });
</script>