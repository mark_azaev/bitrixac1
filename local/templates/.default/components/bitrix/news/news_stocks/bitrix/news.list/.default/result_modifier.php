<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $ID => $arItems){
    $arImage = CFile::ResizeImageGet(
        $arItems['DETAIL_PICTURE'],
            array('width'=>$arParams['LIST_PICTURE_WIDTH'],
                  'height'=>$arParams['LIST_PICTURE_HEIGHT']),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true);

    $arResult['ITEMS'][$ID]['DETAIL_PICTURE'] = $arImage;
}

//test_dump($arResult);

$arTempID = array();
foreach ($arResult['ITEMS'] as $elem)
{
    $arTempID[] = $elem["PROPERTIES"]["LINK_CAT"]["VALUE"];
}
$arSelect = Array("ID", "NAME", "PROPERTY_PRICE","DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>"2", "ACTIVE"=>"Y", "ID" => $arTempID);

$BDResult = CIBlockElement::GetList(
    false,
    $arFilter,
    false,
    false,
    $arSelect);

$arResult["CAT_ELEM"] = array();

while($arRes = $BDResult->GetNext())
{
    $arResult["CAT_ELEM"][$arRes['ID']] = $arRes;
}
?>

