<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDescription = Array(
	"NAME" => GetMessage("RESUME_NAME"),
	"DESCRIPTION" => GetMessage("RESUME_DESC"),
	"ICON" => "",
	"GROUP" => Array("ID" => "personal"),
	"SU_ONLY" => false,
	"SG_ONLY" => false,
	"BLOG_ONLY" => false,
	"AI_ONLY" => false,
);
?>
