<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("form"))
	return false;


$arParameters = Array(
	"PARAMETERS"=> Array(
		"WEB_FORM_ID" => Array(
			"NAME" => GetMessage("GD_WEBFORM_ID"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => '1',
			"REFRESH" => "Y",
		),
	),
	"USER_PARAMETERS"=> Array(
		"PATTERN_ALL" => array(
			"NAME" => GetMessage("GD_PATTERN_ALL"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
        "PATTERN_TODAY" => array(
            "NAME" => GetMessage("GD_PATTERN_TODAY"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),
	),
);

?>