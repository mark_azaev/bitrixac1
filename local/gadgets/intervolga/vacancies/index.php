<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?

#
# INPUT PARAMS
#

$currentDate = (new Bitrix\Main\Type\Date())->format('d.m.Y');

$arGadgetParams["WEB_FORM_ID"] = intval($arGadgetParams["WEB_FORM_ID"]);
if ($arGadgetParams["WEB_FORM_ID"] <= 0)
   return false;

if(!$arGadgetParams["PATTERN_ALL"])
{
    $arGadgetParams["PATTERN_ALL"] = "/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID=".
        $arGadgetParams["WEB_FORM_ID"]."&del_filter=Y";
}

if(!$arGadgetParams["PATTERN_TODAY"])
{
    $arGadgetParams["PATTERN_TODAY"] = "/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID=".
        $arGadgetParams["WEB_FORM_ID"]."&action=list&find_date_create_1=".
        $currentDate."&find_date_create_2=".$currentDate."&set_filter=Y";
}

#
# CACHE
#

$obCache = new CPageCache;
$cacheTime = 3600;
$cacheId = $arGadgetParams["WEB_FORM_ID"];

if($obCache->StartDataCache($cacheTime, $cacheId, "/"))
{
    if(!CModule::IncludeModule("form"))
    {
        ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
        return;
    }

    $arGadgetParams["PATTERN_ALL"] = str_replace(
        "#ID#",
        $arGadgetParams["WEB_FORM_ID"],
        $arGadgetParams["PATTERN_ALL"]
    );

    $arGadgetParams["PATTERN_TODAY"] = str_replace(
            Array("#ID#","#DATE#"),
            Array($arGadgetParams["WEB_FORM_ID"],$currentDate),
            $arGadgetParams["PATTERN_TODAY"]
    );

    $arFilter = Array("DATE_CREATE_1" => $currentDate, "DATE_CREATE_2" => $currentDate);
    $rsResults = CFormResult::GetList(
        $arGadgetParams["WEB_FORM_ID"],
        ($by="s_timestamp"),
        ($order="desc"),
        $arFilter,
    );
    $currentDateRows = $rsResults->SelectedRowsCount();


    ?>

    <?=GetMessage("PATTERN_ALL_DESC")?>
    <a href="<?=$arGadgetParams["PATTERN_ALL"]?>">
        <?=CFormResult::GetCount($arGadgetParams["WEB_FORM_ID"]);?>
    </a>
    <br>
    <?=GetMessage("PATTERN_TODAY_DESC")?>
    <a href="<?=$arGadgetParams["PATTERN_TODAY"]?>">
        <?=$currentDateRows;?>
    </a>

    <?php
    $obCache->EndDataCache();
}