<?php
define("ID_CATALOG", 2);
define("ID_NEWS", 1);
define("ID_STOCKS", 7);
define("ID_GROUP_ADMINS", 1);
define("ID_GROUP_CONTENT_MANAGERS", 5);
define("S1_SITE", "s1");
define("GADGET_PAGE", "gadgetAll");
define("VACANCY_CODE", "VACANCY");

if(file_exists($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/functions.php")){
    require_once ($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/functions.php");
}

if(file_exists($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/agent_check_date.php")){
    require_once ($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/agent_check_date.php");
}

if(file_exists($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/event_handlers.php")){
    require_once ($_SERVER['DOCUMENT_ROOT']."/local/php_interface/include/event_handlers.php");
}


?>
