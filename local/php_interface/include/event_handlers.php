<?php
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("BeforeElementUpdate", "OnBeforeIBlockElementUpdateHandler"));

/**
 * Класс обёртка для функции OnBeforeIBlockElementUpdateHandler
 */
class BeforeElementUpdate
{
    /**
     * Запускается до обновления элемента, если деактивируется элемент ИБ Новости,
     * созданный меньше, чем 3 дня назад, то отменяет деактивацию и выводить предупреждение
     * «Вы деактивировали свежую новость»
     *
     * @param $arFields
     * @return false|void
     */
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"] == ID_NEWS)
        {
            if(CModule::IncludeModule("iblock"))
            {
                $arSelect = Array("ID", "ACTIVE");
                $arFilter = Array("IBLOCK_ID" => ID_NEWS, "ID" => $arFields['ID']);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if ($arRes = $res->GetNext())
                    $arFields["ACTIVE_BEFORE"] = $arRes["ACTIVE"];

                if ($arFields["ACTIVE"] == "N" and $arFields["ACTIVE_BEFORE"] == "Y")
                {
                    $siteFormat = CSite::GetDateFormat();
                    $timeRightNow = MakeTimeStamp(ConvertTimeStamp(), $siteFormat);
                    $timeCreationRecord = MakeTimeStamp($arFields["ACTIVE_FROM"], $siteFormat);

                    $differenceInDays = abs($timeRightNow - $timeCreationRecord) / (24 * 60 * 60);
                    if ($differenceInDays < 3)
                    {
                        global $APPLICATION;
                        $APPLICATION->throwException("Вы деактивировали свежую новость, изменения не применились");
                        return false;
                    }
                }
            }
        }
    }
}

AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("BeforeElementDelete", "OnBeforeIBlockElementDeleteHandler"));

/**
 * Класс обёртка для функции OnBeforeIBlockElementDeleteHandler
 */
class BeforeElementDelete
{
    /**
     * Запускается перед удалением объекта, если количество просмотров товара (поле SHOW_COUNTER) больше 1,
     * то отменяется удаление, деактивируется товар.
     *
     * @param $ID
     * @return false|void
     */
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        if(CModule::IncludeModule("iblock"))
        {
            $arSelect = Array("ID", "SHOW_COUNTER", "IBLOCK_ID");
            $arFilter = Array("ID" => $ID);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arRes = $res->GetNext())
            {
                if ($arRes["IBLOCK_ID"] == ID_CATALOG && $arRes["SHOW_COUNTER"] > 1)
                {
                    (new CIBlockElement())->Update($ID, ['ACTIVE'=>'N']);
                    $GLOBALS['DB']->Commit();

                    $counter = $arRes["SHOW_COUNTER"];
                    global $APPLICATION;
                    $APPLICATION->throwException("Элемент с ID=" . $ID . " нельзя удалить. Количество просмотров: " . $counter);
                    return false;
                }
            }
        }
    }
}

AddEventHandler("main", "OnBeforeUserUpdate", Array("BeforeUserUpdate", "OnBeforeUserUpdateHandler"));

/**
 * Класс обёртка для функции OnBeforeUserUpdateHandler
 */
class BeforeUserUpdate
{
    /**
     * Запускается до обновления пользователя.
     * При добавлении пользователя в группу «Контент-редакторы» отправляются уведомления на email
     * других пользователей группы Контент-редакторы.
     *
     * @param $arFields
     */
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        foreach ($arFields["GROUP_ID"] as $field)
        {
            if($field["GROUP_ID"] == ID_GROUP_CONTENT_MANAGERS)
            {
                $arGroupsForUser = CUser::GetUserGroup($arFields['ID']);
                if(!in_array($field["GROUP_ID"], $arGroupsForUser))
                {
                    $arPar["FIELDS"] = Array("ID", "EMAIL");
                    $filter = Array("GROUPS_ID"=>ID_GROUP_CONTENT_MANAGERS);
                    $rsUsers = CUser::GetList(($by="ID"), ($order="ASC"), $filter, $arPar);

                    $arEmail = Array();
                    while($arUser = $rsUsers->GetNext())
                    {
                        $arEmail[] = $arUser["EMAIL"];

                    }
                    $arEventFields = Array(
                        "TEXT" => $arFields['NAME']." ".$arFields['LAST_NAME']." (".$arFields['ID'].")",
                        "EMAIL" => implode(", ", $arEmail),
                    );

                    CEvent::Send("NEW_CONTENT_MANAGER", S1_SITE, $arEventFields);
                }
            }
        }
    }
}
