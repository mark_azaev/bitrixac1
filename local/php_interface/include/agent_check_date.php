<?php
function AgentCheckDate()
{
    if(CModule::IncludeModule("iblock"))
    {
        $arSelect = Array("ID","NAME","ACTIVE","DATE_ACTIVE_FROM","DATE_ACTIVE_TO");
        $arFilter = Array("IBLOCK_ID" => ID_STOCKS,"!ACTIVE_DATE" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        $arItems = Array();
        while ($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arItems[] = $arFields;
        }

        if(count($arItems)>0)
        {
            CEventLog::Add(Array(
                "SEVERITY"=>"SECURITY",
                "AUDIT_TYPE_ID"=>"CHECK_DATE",
                "MODULE_ID"=>"iblock",
                "ITEM_ID"=>"",
                "DESCRIPTION"=>"Наличие акций с истекшей датой активности: ".count($arItems)
            ));

            $arPar["FIELDS"] = Array("ID", "EMAIL");
            $filter = Array("GROUPS_ID"=>ID_GROUP_ADMINS);
            $rsUsers = CUser::GetList(($by="ID"), ($order="ASC"), $filter, $arPar);

            $arEmail = Array();
            while($arUser = $rsUsers->GetNext())
            {
                $arEmail[] = $arUser["EMAIL"];
            }

            if(count($arEmail) > 0)
            {
                $arEventFields = Array(
                    "TEXT" => count($arItems),
                    "EMAIL" => implode(", ", $arEmail),
                );

                CEvent::Send("CHECK_DATE", S1_SITE, $arEventFields);
            }
        }
    }
    return __METHOD__ . '();';
}
