<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Рабочий стол");
?><?$APPLICATION->IncludeComponent(
	"bitrix:desktop", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"ID" => "holder1s1",
		"CAN_EDIT" => "Y",
		"COLUMNS" => "1",
		"GADGETS" => array(
			0 => "ALL",
		),
		"G_RSSREADER_CACHE_TIME" => "3600",
		"G_RSSREADER_SHOW_URL" => "N",
		"G_RSSREADER_PREDEFINED_RSS" => "",
		"GU_RSSREADER_TITLE_STD" => "",
		"GU_RSSREADER_CNT" => "10",
		"GU_RSSREADER_RSS_URL" => "",
		"GU_RSSREADER_IS_HTML" => "N",
		"GU_HTML_AREA_TITLE_STD" => "",
		"GU_FAVORITES_TITLE_STD" => "",
		"G_PROBKI_CACHE_TIME" => "3600",
		"G_PROBKI_SHOW_URL" => "N",
		"GU_PROBKI_TITLE_STD" => "",
		"GU_PROBKI_CITY" => "c213",
		"G_WEATHER_CACHE_TIME" => "3600",
		"G_WEATHER_SHOW_URL" => "N",
		"GU_WEATHER_TITLE_STD" => "",
		"GU_WEATHER_COUNTRY" => "",
		"GU_WEATHER_CITY" => "",
		"G_VACANCIES_WEB_FORM_ID" => "1",
		"GU_VACANCIES_TITLE_STD" => "Гаджет",
		"GU_VACANCIES_PATTERN" => "",
		"COLUMN_WIDTH_0" => "33%",
		"COLUMN_WIDTH_1" => "33%",
		"COLUMN_WIDTH_2" => "33%",
		"GU_VACANCIES_PATTERN_ALL" => "/gadgetAll/#ID#/",
		"GU_VACANCIES_PATTERN_TODAY" => "/gadgetAll/#ID#/?date=#DATE#"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>